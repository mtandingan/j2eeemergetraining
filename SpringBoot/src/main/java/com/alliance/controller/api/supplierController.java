package com.alliance.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.alliance.entity.Supplier;
import com.alliance.service.supplierService;

@RestController(value = "supplierController")
@RequestMapping(value = "/api/supplier")
public class supplierController {

	@Autowired
	public supplierService supplierservice;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Supplier> getAllSupplier(){
		return supplierservice.getAllSupplier();
	}
	
	@RequestMapping(value = "/addSupplier", method = RequestMethod.POST)
	public void addSupplier() {
		String supplierName = "Jacob's TINDAHAN";
		String supplierAddress = "Switzerland";
		
		Supplier supplier = new Supplier();
		supplier.setSupplierName(supplierName);
		supplier.setSupplierAddress(supplierAddress);
		
		supplierservice.addSupplier(supplier);
	}
	
	@RequestMapping(value = "/updateSupplier", method = RequestMethod.POST)
	public void updateSupplierById() {
		int supplierCode = 5;
		String supplierName = "Mila's Company";
		String supplierAddress = "Malaysia";
		
		Supplier supplier = new Supplier();
		supplier.setSupplierCode(supplierCode);
		supplier.setSupplierName(supplierName);
		supplier.setSupplierAddress(supplierAddress);
		
		supplierservice.updateSupplierById(supplier);
	}
	
	@RequestMapping(value = "/removeSupplier", method = RequestMethod.POST)
	public void removeSupplierById() {
		int supplierCode = 5;
		
		Supplier supplier = new Supplier();
		supplier.setSupplierCode(supplierCode);
		
		supplierservice.removeSupplierById(supplier);
	}
	
}
