package com.alliance.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Type;
import com.alliance.service.typeService;

@RestController(value = "typeController")
@RequestMapping(value = "/api/type")
public class typeController {

	@Autowired
	public typeService typeservice;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Type> getAllSupplier(){
		return typeservice.getAllType();
	}
	
	@RequestMapping(value = "/addType", method = RequestMethod.POST)
	public void addType() {
		String typeName = "Liquor";
		
		Type type = new Type();
		type.setTypeName(typeName);
		
		typeservice.addType(type);
	}
	
	@RequestMapping(value = "/updateType", method = RequestMethod.POST)
	public void updateTypeById() {
		int typeCode = 5;
		String typeName = "Beverages";
		
		Type type = new Type();
		type.setTypeCode(typeCode);
		type.setTypeName(typeName);
		
		typeservice.updateTypeById(type);
	}
	
	@RequestMapping(value = "/removeType", method = RequestMethod.POST)
	public void removeTypeById() {
		int typeCode = 5;
		
		Type type = new Type();
		type.setTypeCode(typeCode);
		
		typeservice.removeTypeById(type);
	}
	
}
