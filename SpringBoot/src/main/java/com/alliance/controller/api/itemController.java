package com.alliance.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alliance.entity.Item;
import com.alliance.entity.Supplier;
import com.alliance.entity.Type;
import com.alliance.service.itemService;

@RestController(value = "itemController")
@RequestMapping(value = "/api/item")
public class itemController {

	@Autowired
	public itemService itemservice;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Item> getAllItem(){
		return itemservice.getAllItem();
	}
	
	@RequestMapping(value = "addItem", method = RequestMethod.POST)
	public void addItem(){
		Item item = new Item();
		
		int itemLimit = 20;
		int itemQty = 30;
		String itemName = "Chicken Nuggets";
		
		Supplier supplier = new Supplier();
		Type type = new Type();
		
		supplier.setSupplierCode(1);
		type.setTypeCode(1);
		
		item.setItemLimit(itemLimit);
		item.setItemName(itemName);
		item.setItemQty(itemQty);
		item.setSupplier(supplier);
		item.setType(type);
		
		itemservice.addItem(item);
	}
	
	@RequestMapping(value = "updateItem", method = RequestMethod.POST)
	public void updateItem(){
		Item item = new Item();
		
		int itemLimit = 10;
		int itemQty = 20;
		String itemName = "Chicken Nuggets";
		
		Supplier supplier = new Supplier();
		Type type = new Type();
		
		supplier.setSupplierCode(1);
		type.setTypeCode(2);
		
		item.setItemCode(5);
		item.setItemLimit(itemLimit);
		item.setItemName(itemName);
		item.setItemQty(itemQty);
		item.setSupplier(supplier);
		item.setType(type);
		
		itemservice.updateItem(item);
	}
	
	@RequestMapping(value = "updateLimit", method = RequestMethod.POST)
	public void updateLimitById(){
		Item item = new Item();
		int itemLimit = 15;
		
		item.setItemCode(5);
		item.setItemLimit(itemLimit);
		
		itemservice.updateLimitById(item);
	}
	
	@RequestMapping(value = "deleteItem", method = RequestMethod.POST)
	public void deleteItem(){
		Item item = new Item();
		item.setItemCode(4);
		
		itemservice.deleteItem(item);
	}
	
}
