package com.alliance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.alliance.entity.Item;

@Repository("itemRepository")
public interface itemRepository extends JpaRepository<Item, String> {

	@Modifying
	@Query("UPDATE #{#entityName} i SET i.itemLimit = ?2 WHERE i.itemCode = ?1 ")
	void updateLimitById(int itemCode, int itemLimit);

}
