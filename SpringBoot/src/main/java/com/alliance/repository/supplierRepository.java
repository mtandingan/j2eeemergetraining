package com.alliance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.alliance.entity.Supplier;
import com.alliance.entity.Type;

@Repository("supplierRepository")
public interface supplierRepository extends JpaRepository<Supplier, Integer> {
	
}
