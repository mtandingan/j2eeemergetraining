package com.alliance.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.core.serializer.Serializer;

@Entity
@Table(name = "Item", schema = "inventory", catalog = "")
public class Item implements Serializable{
	
	@Id @GeneratedValue
	@Column(name = "itemCode")
	int itemCode;
	
	@Basic
	@Column(name = "itemQty")
	int itemQty;
	
	@Basic
	@Column(name = "itemLimit")
	int itemLimit;
	
	@Basic
	@Column(name = "itemName")
	String itemName;
	
	@ManyToOne
	@JoinColumn(name = "supplierCode")
	private Supplier supplier;
	
	@ManyToOne
	@JoinColumn(name = "typeCode")
	private Type type;
	
	public Item() {
		super();
	}

	public int getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(int itemCode) {
		this.itemCode = itemCode;
	}
	
	public int getItemQty() {
		return itemQty;
	}
	
	public void setItemQty(int itemQty) {
		this.itemQty = itemQty;
	}
	
	public int getItemLimit() {
		return itemLimit;
	}
	
	public void setItemLimit(int itemLimit) {
		this.itemLimit = itemLimit;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
}
