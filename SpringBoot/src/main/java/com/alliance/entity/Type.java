package com.alliance.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Type", schema = "inventory", catalog = "")
public class Type implements Serializable {
	
	@Id @GeneratedValue
	@Column(name = "typeCode", length = 11, nullable = false)
	int typeCode;
	
	@Basic
	@Column(name = "typeName", length = 45, nullable = false)
	String typeName;

	public Type() {
		super();
	}

	public int getTypeCode() {
		return typeCode;
	}
	
	public void setTypeCode(int typeCode) {
		this.typeCode = typeCode;
	}
	
	public String getTypeName() {
		return typeName;
	}
	
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
