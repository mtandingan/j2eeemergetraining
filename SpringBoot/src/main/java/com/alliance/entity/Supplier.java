package com.alliance.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Supplier", schema = "inventory", catalog = "")
public class Supplier implements Serializable{

	@Id @GeneratedValue
	@Column(name = "supplierCode", length = 11, nullable = false)
	int supplierCode;
	
	@Basic
	@Column(name = "supplierName", length = 50, nullable = true)
	String supplierName;
	
	@Basic
	@Column(name = "supplierAddress", length = 50, nullable = true)
	String supplierAddress;
	
	public Supplier() {
		super();
		
	}

	public int getSupplierCode() {
		return supplierCode;
	}
	
	public void setSupplierCode(int supplierCode) {
		this.supplierCode = supplierCode;
	}
	
	public String getSupplierName() {
		return supplierName;
	}
	
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	
	public String getSupplierAddress() {
		return supplierAddress;
	}
	
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	
}
