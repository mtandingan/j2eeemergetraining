package com.alliance.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alliance.entity.Item;
import com.alliance.repository.itemRepository;

@Service("itemService")
@Transactional(rollbackOn = Exception.class)
public class itemService {

	@Autowired
	public itemRepository itemrepository;
	
	public List<Item> getAllItem(){
		return itemrepository.findAll();
	}
	
	public void addItem(Item item) {
		itemrepository.saveAndFlush(item);
	}

	public void updateItem(Item item) {
		itemrepository.saveAndFlush(item);
	}

	public void deleteItem(Item item) {
		itemrepository.delete(item);
	}

	public void updateLimitById(Item item) {
		int itemCode = item.getItemCode();
		int itemLimit = item.getItemLimit();
		
		itemrepository.updateLimitById(itemCode, itemLimit);
	}
}
