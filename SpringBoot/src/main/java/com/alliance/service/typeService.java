package com.alliance.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.entity.Supplier;
import com.alliance.entity.Type;
import com.alliance.repository.supplierRepository;
import com.alliance.repository.typeRepository;

@Service("typeService")
@Transactional(rollbackOn = Exception.class)
public class typeService {

	@Autowired
	public typeRepository typerepository;
	
	public List<Type> getAllType(){
		return typerepository.findAll();
	}
	
	public void addType(Type type) {	
		typerepository.saveAndFlush(type);
	}
	
	public void updateTypeById(Type type) {	
		typerepository.saveAndFlush(type);
	}
	
	public void removeTypeById(Type type){
		typerepository.delete(type);
	}
}
