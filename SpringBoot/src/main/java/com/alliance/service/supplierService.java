package com.alliance.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alliance.entity.Supplier;
import com.alliance.repository.supplierRepository;

@Service("supplierService")
@Transactional(rollbackOn = Exception.class)
public class supplierService {

	@Autowired
	public supplierRepository supplierrepository;
	
	public List<Supplier> getAllSupplier(){
		return supplierrepository.findAll();
	}
	
	public void addSupplier(Supplier supplier) {	
		supplierrepository.saveAndFlush(supplier);
	}
	
	public void updateSupplierById(Supplier supplier) {	
		supplierrepository.saveAndFlush(supplier);
	}
	
	public void removeSupplierById(Supplier supplier){
		supplierrepository.delete(supplier);
	}
}
